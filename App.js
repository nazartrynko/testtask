import React, {useEffect, useReducer} from 'react';
import {SafeAreaView, StatusBar, useColorScheme} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

import {reducer, initialState} from './src/store/reducer';
import {setUsers} from './src/store/actions';
import BaseModal from './src/components/Modals/BaseModal';
import UsersList from './src/components/UsersList';
import {URLS} from './src/constants';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [{users, modal}, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    async function fetchUsers() {
      const data = await fetch(URLS.USERS);
      const {results} = await data.json();

      dispatch(setUsers(results));
    }

    fetchUsers();
  }, []);

  return (
    <>
      <SafeAreaView style={backgroundStyle}>
        <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
        <UsersList users={users} dispatch={dispatch} />
      </SafeAreaView>
      <BaseModal modal={modal} dispatch={dispatch} />
    </>
  );
};

export default App;
