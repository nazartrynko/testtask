### What is this repository? ###

iOS and Android application build with [React Native](https://reactnative.dev/) which fetches random users from [https://reactnative.dev](https://reactnative.dev/) and shows their details in modal window

### How do I get set up? ###

```sh
yarn
```
```sh
cd ios && pod install
```

### How to start project? ###

### iOS

```sh
yarn ios
```

### Android

```sh
yarn android
```

p.s. you might need to check the list of emulators with ```emulator -list-avds``` and run one of them before ```yarn android``` with command ```emulator -avd <name-of-emulator>```