export const types = {
  SHOW_MODAL: 'SHOW_MODAL',
  HIDE_MODAL: 'HIDE_MODAL',
  SET_USERS: 'SET_USERS',
  SET_SELECTED_USER: 'SET_SELECTED_USER',
};
