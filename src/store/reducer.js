import {types} from './types';

export const initialState = {
  modal: null,
  users: [],
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.SHOW_MODAL:
      return {
        ...state,
        modal: {
          modalType: action.payload.modalType,
          modalPayload: action.payload.modalPayload,
        },
      };
    case types.HIDE_MODAL:
      return {...state, modal: false};
    case types.SET_USERS:
      return {...state, users: action.payload.users};
    case types.SET_SELECTED_USER:
      return {...state, selectedUser: action.payload.selectedUser};
    default:
      throw new Error();
  }
};
