import {types} from './types';

export const setUsers = users => ({
  type: types.SET_USERS,
  payload: {
    users,
  },
});

export const setSelectedUser = selectedUser => ({
  type: types.SET_SELECTED_USER,
  payload: {
    selectedUser,
  },
});

export const showModal = (modalType, modalPayload) => ({
  type: types.SHOW_MODAL,
  payload: {
    modalType,
    modalPayload,
  },
});

export const hideModal = () => ({
  type: types.HIDE_MODAL,
});
