import React, {useCallback} from 'react';
import {FlatList, StyleSheet} from 'react-native';

import {showModal} from '../store/actions';
import ListItem from './ListItem';
import {modalTypes} from './Modals/constants';

const UsersList = ({users, dispatch}) => {
  const renderItem = useCallback(
    ({item}) => (
      <ListItem
        item={item}
        onPress={() => {
          dispatch(showModal(modalTypes.USER_DETAILS, item));
        }}
      />
    ),
    [],
  );

  return (
    <FlatList
      style={styles.list}
      data={users}
      renderItem={renderItem}
      keyExtractor={item => item.cell}
    />
  );
};

const styles = StyleSheet.create({
  list: {height: '100%'}
});

export default UsersList;
