import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const ListItem = ({item, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <>
        <Image style={styles.avatar} source={{uri: item.picture.thumbnail}} />
        <View style={styles.infoContainer}>
          <View style={styles.nameWrapper}>
            <Text style={styles.firstName}>{item.name.first}</Text>
            <Text>{item.name.last}</Text>
          </View>
          <Text style={styles.phone}>{`phone: \n${item.phone}`}</Text>
        </View>
      </>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    marginVertical: 2,
    paddingHorizontal: 5,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: '#c9ccd1',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
  },
  avatar: {
    height: 30,
    width: 30,
    borderRadius: 50,
    marginRight: 10,
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameWrapper: {flexDirection: 'row'},
  firstName: {marginRight: 5},
  phone: {width: 150, fontSize: 10, fontStyle: 'italic'},
});

export default ListItem;
