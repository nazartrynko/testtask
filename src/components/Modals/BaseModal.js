import {BlurView} from '@react-native-community/blur';
import React, {useCallback} from 'react';
import {Image, StyleSheet, TouchableOpacity, Platform} from 'react-native';

import {hideModal} from '../../store/actions';
import {modalTypes} from './constants';
import UserInfoModal from './UserInfoModal';

const modalComponents = {
  [modalTypes.USER_DETAILS]: UserInfoModal,
};

const BaseModal = ({modal, dispatch}) => {
  if (!modal) return null;

  const {modalType, modalPayload} = modal;
  const Modal = modalComponents[modalType];

  const handleClose = useCallback(() => {
    dispatch(hideModal());
  }, []);

  return (
    modal && (
      <>
        <BlurView
          style={styles.blur}
          blurType="dark"
          blurAmount={20}
          reducedTransparencyFallbackColor="white"
        />
        <Modal modalPayload={modalPayload} />
        <TouchableOpacity style={styles.close} onPress={handleClose}>
          <Image
            source={require('../../cross.png')}
            style={{height: 25, width: 25, tintColor: 'white'}}
          />
        </TouchableOpacity>
      </>
    )
  );
};

const styles = StyleSheet.create({
  blur: {
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
  close: {
    position: 'absolute',
    right: 20,
    top: Platform.OS === 'ios' ? 50 : 20,
  },
  closeIcon: {
    height: 25,
    width: 25,
    tintColor: 'white',
  },
});

export default BaseModal;
