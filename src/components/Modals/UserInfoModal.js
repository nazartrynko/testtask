import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const UserInfoModal = ({modalPayload: {email, location, dob, gender}}) => (
  <View style={styles.container}>
    <View style={[styles.detailedInfoContainer, styles.bottomBorder]}>
      <Text style={styles.detailedInfoLabel}>Email</Text>
      <Text style={{flex: 1}}>{email}</Text>
    </View>
    <View style={[styles.detailedInfoContainer, styles.bottomBorder]}>
      <Text style={styles.detailedInfoLabel}>City</Text>
      <Text>{location.city}</Text>
    </View>
    <View style={[styles.detailedInfoContainer, styles.bottomBorder]}>
      <Text style={styles.detailedInfoLabel}>Date of birth</Text>
      <Text>{new Date(dob.date).toLocaleDateString()}</Text>
    </View>
    <View style={styles.detailedInfoContainer}>
      <Text style={styles.detailedInfoLabel}>Gender</Text>
      <Text>{gender}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    width: '90%',
    height: 170,
    backgroundColor: '#f2f4f7',
    position: 'absolute',
    top: '30%',
    alignSelf: 'center',
    borderRadius: 10,
    paddingVertical: 5,
  },
  detailedInfoContainer: {
    flexDirection: 'row',
    padding: 10,
  },
  bottomBorder: {
    borderBottomWidth: 1,
    borderBottomColor: '#c9ccd1',
  },
  detailedInfoLabel: {
    width: 90,
    marginRight: 10,
  },
});

export default UserInfoModal;
